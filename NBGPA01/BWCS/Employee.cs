﻿using System;

namespace BWCS.Employees
{
    public enum State
    {
        PENDING, AVAILABLE
    } 
    public class Employee
    {
        private int memberId;
        private string fName;
        private string lName;
        private DateTime dob;
        private State entryState;

        string format;

        public Employee()
        {
            memberId = 0;
            fName = "";
            lName = "";
            dob = DateTime.Today;
            this.entryState = State.AVAILABLE;
            format = "MM-DD-YYYY";
        }

        public Employee(int ID, string fName, string lName, string dob)
        {
            memberId = ID;
            this.fName = fName;
            this.lName = lName;
            this.dob = DateTime.Parse(dob);
        }

        public int MemberId
        {
            get { return memberId; }
            set { memberId = value; }            
        }

        public string FName
        {
            get { return fName; }
            set { fName = value; }
        }

        public string LName
        {
            get { return lName; }
            set { lName = value; }
        }

        public string DOB
        {
            get { return dob.ToString(format); }
            set { dob = DateTime.Parse(value); }
        }

        public State EntryState
        {
            get { return entryState; }
            set
            {
                if (value == State.AVAILABLE || value == State.PENDING)
                {
                    entryState = value; 
                }
            }
        }

        public string makeString()
        {
            string obj = "";
            if (MemberId != 0)
            {
                obj = MemberId.ToString();
            }
            obj += "," + FName + "," + LName + "," + DOB;
            return obj;
        }
    }
}
