﻿/*
File: Messenger.cs
Project: NBGPA01
Developer(s): Nathan Bray and Gabriel Paquette
Date created: September 20, 2016
Description: The messenger class deals with either parsing out a message sent, creating
             messages to be sent, or formatting messages to be sent out.
*/
using System;
using System.IO;

namespace BWCS.Messenger
{
    public enum Status
    {
        IDLE = 0,
        ADD = 1,
        UPDATE = 2,
        FIND = 3,
        DELETE = 4,
        ADMIN = 5,
        ERROR = 6,
        DBSAVE = 7,
        EXIT = 8,
        SPAM = 9
    }


    /*
    Name: Messenger
    Developer(s):Gabrel Paquette and Nathan Bray
    Description: this class deals with creating and formatting messages
                 that will be sent to the server.
    */
    public class Messenger
    {
        public const int ARGUMENTCOUNT = 5;
        private static Status status;
        private static string dateFormat = "MM-dd-yyyy";

        private static Random rand = new Random();

        /*
        Name: Messenger()
        Description: sets the message status to idle when a messenger class
                     object is instantiated.
        */
        public Messenger()
        {
            status = Status.IDLE;
        }


        /*
        Name: Status
        Description: gets and gets the value of our status ENUM
        Return: returns status ENUM
        */
        public static Status Status
        {
            get { return status; }
            set { status = value; }
        }


        /*
        Name: stressTest
        Description: This method generates a random first name, last name,
                     and dob which is then formatted and returned to be 
                     sent to the server.
        Return: returns a pre-formatted message containing information to 
                create a new client in the database
        */
        public static string stressTest()
        {

            string randFirstName = "";
            string randLastName = "";

            DateTime start = new DateTime(1900, 1, 1);
            DateTime date;
            int dateRange = 0;

            string message = "";

            //creates a random first name 
            randFirstName = randomNameGeneration();
            //creates a random last name 
            randLastName = randomNameGeneration();

            //generates a random date between 1900 jan 1st and today
            dateRange = (DateTime.Today - start).Days;
            date = start.AddDays(rand.Next(dateRange));

            //formats the information in a way so that the server can read it
            message = makeMessage(Status.SPAM, "", randFirstName, randLastName, date);

            //returns the foramtted message
            return message;
        }

        /*
        Name: parseMessage 
        Description: takes the message sent from the client and parses through it
                     to take out the first name, last name, date of birth, and client ID
        Parameters: string msg-> this is the message sent from the client
        Return: returns the array of strings that contains the parsed out message
        */
        public static string[] parseMessage(string msg)
        {

            string[] response = new string[5];
            if (msg != null)
            {
                char[] delimeter = new char[1] { ',' };
                response = msg.Split(delimeter, StringSplitOptions.RemoveEmptyEntries); 
            }
            return response;
        }


        /*
        Name: makeMessage
        Description: gets passed the first name, last name, date of birth, ID and
                     and an action code, and compiles it into a message being coma delimited
        Parameters: Status actionCode -> this is an number between 0 and 8, which indicates to
                                         the server what action to do with the information provided
                    string memberID -> this is the ID of the client being worked on, if no ID is 
                                       available, then it is 0
                    string fName -> this is the first name of the client
                    string lName -> this is the last name of the client
                    DateTime dob -> this is the date of birth of the client
        Return: returns a compliled message that contains all of the information of the client,
                which is coma delimited
        */
        public static string makeMessage(Status actionCode, string memberID, string fName, string lName, DateTime dob)
        {
            string message = "";
            message = (int)actionCode + "," + memberID + ","
                + fName + "," + lName + "," + dob.ToString(dateFormat);
            return message;
        }      

        
        /*
       Name: responseFormat 
       Description: this returns a formatted message that contains all 
                    information about the client
       Parameters: string[] contents -> an array which contains the name, dob 
                                        of the client
       Return: returns a correctly formatted message for the client to see
       */
        public static string responseFormat(string[] contents)
        {
            int state = Convert.ToInt32(contents[0]);
            string msg = "";
            switch ((Status)state)
            {
                case Status.ERROR:
                    msg = contents[1];
                    break;
                case Status.UPDATE:
                    string memID = contents[1];
                    string oldFName = contents[2];
                    string oldLName = contents[3];
                    string oldDOB = contents[4];
                    string newFName = contents[5];
                    string newLName = contents[6];
                    string newDOB = contents[7];

                    DateTime oldDate = DateTime.Parse(oldDOB);
                    oldDOB = oldDate.ToString("MMMM.dd, yyyy");
                    DateTime newDate = DateTime.Parse(newDOB);
                    newDOB = newDate.ToString("MMMM.dd, yyyy");

                    msg += string.Format("{0,-19} {1,-19}\n", "Member ID:", memID);
                    msg += string.Format("{0,-19} {1,-19} => {2,-19} {3,-19}\n", "Old First Name:", oldFName, "New First Name:", newFName);
                    msg += string.Format("{0,-19} {1,-19} => {2,-19} {3,-19}\n", "Old Last Name:", oldLName, "New Last Name:", newLName);
                    msg += string.Format("{0,-19} {1,-19} => {2,-19} {3,-19}\n", "Old Date of Birth:", oldDOB, "New Date of Birth:", newDOB);
                    break;
                case Status.ADD:
                case Status.FIND:
                    string id = contents[1];
                    string fName = contents[2];
                    string lName = contents[3];
                    string dob = contents[4];

                    DateTime dt = DateTime.Parse(dob);
                    dob = dt.ToString("MMMM.dd, yyyy");
                    msg = "Member ID:\t" + id;
                    msg += "\nFirst Name:\t" + fName;
                    msg += "\nLast Name:\t" + lName;
                    msg += "\nDate of Birth:\t" + dob;
                    break;
                case Status.DELETE:
                    msg = "Entry successfully Removed";
                    break;
                default:
                    break;
            }
            return msg; 
        }


        /*
        Name: randomNameGeneration
        Description: Generates a random name which contains
                     letters and numbers
        Parameters:n/a
        Return: returns a random 8 character alphanumaric word.
        */
        public static string randomNameGeneration()
        {
            //generates a random 12 character string
            string randomString = Path.GetRandomFileName();

            /*
            returns the first 8 characters of the string.
            This is because Path.GetRandomFileName() puts
            a comma into the 9th index of the string that
            it creates
            */
            return randomString.Substring(0,8);
        }
    }
}
