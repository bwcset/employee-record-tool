﻿/*
 * File         : TCPHandler.cs
 * Project      : NBGPA01
 * Developer(s) : Nathan Bray and Gabriel Paquette
 * Date Created : September 20, 2016
 * Description  : This file contains anything to do with sending and recieving
 *                messages to and from the server. It also contains a way to 
 *                determine the local IP address of the computer.
 */
using System;
using System.IO;
using System.Net.Sockets;
using System.Net;

namespace BWCS.TCPHandler
{

    /*
    Name: TCPHandler
    Developer(s):Gabrel Paquette and Nathan Bray
    Decription: This class contains everything that deals with sending and recieving
                messages to and from the server. As well as aquiring the local IP
                address for the server.
    */
    public class TCPHandler
    {
        private static string serverIp = "127.0.0.1"; //local host IP
        private const int port = 8001; //the port used for the client and server
        private static int spamAmount = 5000; //the number of clients created when
                                              //executing the stress test on the server


        /*
        Name: ServerIp
        Description: gets and sets the value of the server IP
        Parameters:N/A
        Return: serverIp
        */
        public static string ServerIp
        {
            get { return serverIp; }
            set { serverIp = value.ToString(); }
        }


        /*
        Name: SpamAmount
        Description: this method contains way to get and set the 
                     value for spamAmount, which is the number
                     of clients to added when running the stress test
        Parameters: N/A
        Return: spamAmount
        */
        public static int SpamAmount
        {
            get { return spamAmount; }
            set { spamAmount = value; }
        }

        /*
        Name: sendMsg
        Description: connects to the server and sends a preformated message which can
                     contain the first name, last name, dob, ID and action code
        Parameters: string msg -> this is the formatted message
        Return: returns the server response as a string
        */
        public static string sendMsg(string msg)
        {
            string receivedMsg = String.Empty;

            try
            {
                //conects the client to the server
                TcpClient client = new TcpClient(serverIp, port);
                Console.WriteLine("Connected!");

                //gets streams to write to and read from
                StreamWriter writer = new StreamWriter(client.GetStream());
                StreamReader reader = new StreamReader(client.GetStream());

                //sends the message to the server
                Console.Write("Transmitting . . .");
                writer.WriteLine(msg);
                writer.Flush();
                //reads the message sent back from the server
                receivedMsg = reader.ReadLine();
                //closes the connection with the server
                client.Close();

            }
            catch (Exception e)
            {
                receivedMsg = (int)Messenger.Status.ERROR + "," + e.Message;
            }
            
            //the final message sent by the server
            return receivedMsg;
        }



        /*
       Name: sendMsg
       Description: connects to the server and sends a rendomly generated,
                    pre-formated message which contains the action code, first name, 
                    last name, and dob. 5000 clients are generated and sent to the 
                    server to be added.
       Parameters: N/A
       Return: returns the server response as a string
       */
        public static string sendStressMsg()
        {
            string receivedMsg = String.Empty;
            string msg = String.Empty;

            try
            {
                //conects the client to the server
                TcpClient client = new TcpClient(serverIp, port);
                Console.WriteLine("Connected!");

                //gets streams to write to and read from
                StreamWriter writer = new StreamWriter(client.GetStream());
                StreamReader reader = new StreamReader(client.GetStream());

                //loops 5000 times, creating randomly generated first names
                //last names, dobs and then formats them.
                for (int i = 0; i < TCPHandler.SpamAmount; i++)
                {
                    //gets the random client info
                    msg = Messenger.Messenger.stressTest();
                    Console.Write("Transmitting . . .");
                    //sends the info to the server
                    writer.WriteLine(msg);
                    writer.Flush();
                    
                    receivedMsg = reader.ReadLine();
                    if (receivedMsg.Contains("6, "))
                    {
                        break;
                    }
                }
                
                client.Close();

            }
            catch (Exception e)
            {
                receivedMsg = (int)Messenger.Status.ERROR+"," + e.Message;
            }

            //this is the final message sent by the server
            return receivedMsg;
        }


        /*
        Name: GetLocalIPAddress 
        Description: gets the ip address of the local computer
                     taken from a stack overflow answer here:
                     http://stackoverflow.com/questions/6803073/get-local-ip-address
        Parameters: n/a
        Return: returns the ip of the local computer as a string
        */
        public static string GetLocalIPAddress()
        {
            string ipAddress = "";
            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ip.ToString();
                    }
                }
            }
            catch (Exception)
            {
                ipAddress = "127.0.0.1";
            }
            serverIp = ipAddress;
            return ipAddress;
        }
    }
}
