﻿/*
File: DataStructure.cs
Project: NBGPA01
Developer(s): Nathan Bray and Gabriel Paquette
Date created: September 20, 2016
Description: this file contains the class for the database. Everything that has to
             do with the database and its functionality is contain in here.
*/


using System;
using System.Collections;
using BWCS.Messenger;

namespace GPNBA01Server
{
    /*
    Name: DataStructure
    Developer(s):Gabrel Paquette and Nathan Bray
    Decription: This class contains the database container, as well as all the 
                functionality that goes along with the database, ie. add, update
                delete, find.
    */
    class DataStructure
    {
        private int masterID; //the value which determines what the next memberID will be
        private const int recordCount = 40000; //the max number of entry that are allowed in the database
        private object myLock; //the object used to lock the masterID
        private Hashtable records; //the database


        /*
        Name: DataStructure
        Description: this is the constructor the the DataStucture class
                     it creates a lock object and the hash table that will 
                     contain the database. it also sets the masterID to 0
        */
        public DataStructure()
        {
            myLock = new object();
            records = new Hashtable();
            lock (myLock)
            {
                masterID = 0;
            }
        }


        //returns the hashtabel
        public Hashtable Records
        {
                get { return records; }
        }

        //returns the masterID value 
        //sets the masterID to a specific value
        public int MasterID
        {
            get { return masterID; }
            set { masterID = value; }
        }


        /*
        Name: addEntry
        Description: adds a client to the data base using the parameters 
                     passed to it and then verifies if the client was added 
                     successfully.
        Parameters: string fName-> the first name of the entry to be added
                    string lName-> the last name of the entry to be added
                    string dob -> the date of birth of the entry to be added
        return: returns if the add was successfull or not, and if there was an error
        */
        public string addEntry(string fName, string lName, string dob)
        {

            string response = "";
            int status = (int)Status.ERROR;

            //checks if the database is not at its maximum (40000)
            if (records.Count < recordCount)
            {
                //if its not full, increase the master ID by 1
                masterID += 1;
                try
                {
                    //create an entry with the information that will be added to the database
                    Employee entry = new Employee(masterID, fName, lName, dob);
                    //add the entry to the database
                    records.Add(entry.MemberId, entry);

                    //checks if the entry was added successfully
                    if (records.ContainsKey(entry.MemberId))
                    {
                        //if it was, then create a message which contains the newly added entry
                        response = Messenger.makeMessage(Status.ADD, entry.MemberId.ToString(), fName, lName, DateTime.Parse(dob));
                        //write to the log
                        FileHandler.log(Status.ADD);
                    }
                    else
                    {
                        //if the add failed
                        response = status + ",Error creating entry.";
                        FileHandler.log(Status.ERROR, response);
                    }
                }
                catch (Exception e)
                {
                    //if there was an error while trying to add to the database
                    FileHandler.log(Status.ERROR, e.Message);
                    response = status + "," + e.Message;
                }
            }
            else
            {
                //if the database is at its maximum
                response = status + ", The database has reached it's capacity. Remove entries and try again later";
            }

            //return the responce for the client to see
            return response;
        }


        /*
        Name: addEntry
        Description: adds a client to the data base using the parameters 
                     passed to it and then verifies if the client was added 
                     successfully. this only runs at the start of the program
                     when first loading the database from the file
        Parameters: memberID -> this is the id of the client that is going to be added
                    fName -> first name of the client
                    lName -> last name of the client
                    dob -> the date of birth of the client
        return: returns true if the client was added suuccessfully, and
                false if it was not.
        */
        public bool addEntry(int memberID, string fName, string lName, string dob)
        {
            bool response = true;
            //creates a new entry with the information provided
            Employee entry = new Employee(memberID, fName, lName, dob);

            //checks if the database is not at its maximum (40000)
            if (records.Count < recordCount)
            {
                if (memberID >= masterID)
                {
                    //increases the masterID by 1, so the next 
                    //entry added gets the correct memeberID
                    masterID = memberID + 1;
                }
                try
                {
                    //adds the new entry to the DB
                    records.Add(entry.MemberId, entry);
                    //confirms the entry added successfully
                    if (!records.ContainsKey(entry.MemberId))
                    {
                        response = false;
                    }
                }
                catch (Exception e)
                {
                    //an error happened when adding the entry

                    FileHandler.log(Status.ERROR, e.Message);
                    response = false;
                }
            }

            //returns the success or failure of the add of the entry
            return response;
        }


        /*
        Name: fineEntry
        Description: finds the requested entry in the data base
                     via the memberId provided
        Parameters: the memeber id to be searched for
        return: returns the employee object that was being searched for
        */
        public Employee findEntry(int id)
        {
            //creates a temp employee to be returned
            Employee tempEmployee = new Employee();
            ICollection recordCol = records.Values;

                //checks if the ID provided exists in the database
                if (records.ContainsKey(id) == true)
                {      
                    //iterates through the data base to find the correct 
                    //employee to be returned.
                    foreach (Employee record in recordCol)
                    {
                        if (record.MemberId == id)
                        {
                            //saves the employee info to the temp employee
                            tempEmployee.MemberId = record.MemberId;
                            tempEmployee.FName = record.FName;
                            tempEmployee.LName = record.LName;
                            tempEmployee.DOB = record.DOB;
                            break;
                        }
                    }
                }
            //returns the temp employee with the requested info
            return tempEmployee;
        }


        /*
        Name: findEntry
        Description: finds the requested entry and returns a pre-formatted 
                     message containing the entry info
        Parameters: string id -> this is the id of the employee that is
                                 is being searched for
        return: returns a pre-formatted message containing the entry info
        */
        public string findEntry(string id)
        {
            int status = (int)Status.FIND;
            string msg = status+ ",";
            int ID = Convert.ToInt32(id);
            ICollection recordCol = records.Values;

            //checks if the ID provided exists in the database
            if (records.ContainsKey(ID) == true)
            {
                //iterates through the data base to find the correct employee
                foreach (Employee record in recordCol)
                {

                    if (record.MemberId == ID)
                    {
                        //adds the employee info into a message and foramts it
                        //to be read by the client
                        msg += id + ",";
                        msg += record.FName + ",";
                        msg += record.LName + ",";
                        msg += record.DOB;
                        break;
                    }
                }
            }
            else
            {
                status = (int)Status.ERROR;
                msg = status + ",Unable to Locate entry with the ID of " + id;
            }
            
            //returns the pre-formatted entry information
            return msg;
        }


        /*
        Name: updateEntry
        Description: uses the information passed to it to find and the
                     update the correct entry in the database
        Parameters: string[] elemenets -> this contains all of the new information
                                          for the specified entry
        return: 
        */
        public string updateEntry(string[] elements)
        {
            //creates a temp employee
            Employee oldEmployee = new Employee();

            int state = (int)Status.UPDATE;
            ICollection recordCol = records.Values;

            //converts the entry id to an int
            int memID = Convert.ToInt32(elements[1]);
            string msg = "";
            try
            {   
                //searchs for the specified entry in the data base and gets
                //a reference to the entry
                oldEmployee = findEntry(memID);
                //if the entry couldn't be found, the old employee ID wouldn't 
                //change from 0.
                if (oldEmployee.MemberId != 0)
                {
                    //iterate through the database and find the correct entry
                    foreach (Employee record in recordCol)
                    {
                        //if you find it
                        if (record.MemberId == oldEmployee.MemberId)
                        {
                            //update the information of the record
                            record.MemberId = memID;
                            record.FName = elements[2];
                            record.LName = elements[3];
                            record.DOB = elements[4];
                            break;
                        }
                    }

                    //compile a message with the old and new information of the entry 
                    msg = state +","+ memID + "," + oldEmployee.FName + "," + oldEmployee.LName + "," +
                               oldEmployee.DOB + "," + elements[2] + "," + elements[3] + "," + elements[4];
                    //write to the log
                    FileHandler.log(Status.UPDATE);
                }
                else
                {
                    //if the entry could not be found
                    state = (int)Status.ERROR;
                    FileHandler.log(Status.ERROR, "Could not locate entry to update");
                    msg = state + ",Unable to Locate entry with the ID of " + memID;
                }
            }
            catch (Exception e)
            {
                //write to the log if a error occured
                FileHandler.log(Status.ERROR, e.Message);
                Console.WriteLine(e.Message);
            }

            //returned a pre-formatted message containing the old and new information of the record
            //to show the user what exactly was changed
            return msg;
        }


        /*
        Name: deleteEntry
        Description: confirms the entry exists in the database, removes it
                     and then confirms the entry was removed.
        Parameters: string id -> the id of the entry to be deleted
        return: a message saying if the delete was succesful or not
        */
        public string deleteEntry(string id)
        {
            //creates a temp employee
            Employee temp = new Employee();
            //converts the id to an int
            int memID = Convert.ToInt32(id);
            string response = "";
            int state = (int)Status.ERROR;
            try
            {
                //checks if the entry exists in the database 
                if (records.ContainsKey(memID))
                {
                    //gets a reference to the entry trying to be removed
                    temp = findEntry(memID);
                    //compiles a message with the info of the removed entry to 
                    //show the user what info was deleted
                    response = Messenger.makeMessage(Status.DELETE, temp.MemberId.ToString(), temp.FName, temp.LName, DateTime.Parse(temp.DOB));
                    //removes the entry 
                    records.Remove(memID);

                    //checks if the record was successfully deleted
                    if (!records.ContainsKey(memID))
                    {
                        //if the database does not contain the entry, then it was removed successfully 
                        FileHandler.log(Status.DELETE);
                    }
                    else
                    {
                        //if it does exist, then the delete failled
                        FileHandler.log(Status.ERROR, "Failed to delete entry");
                        response = state + ",Unable to remove entry " + memID;
                    }
                }
                else
                {
                    //if the database does not contain the entry, then it can not remove that entry
                    FileHandler.log(Status.ERROR, "Could not locate entry to remove");
                    response = state + ",Unable to locate entry with the ID of " + memID;
                }
            }
            catch (Exception e)
            {
                FileHandler.log(Status.ERROR, e.Message);
                Console.WriteLine(e.Message);
            }

            //returns a response to the client for the user to see
            return response;
        } 
    }
}
