﻿/*
File: Employee.cs
Project: NBGPA01
Developer(s): Nathan Bray and Gabriel Paquette
Date created: September 20, 2016
Description: This class is what is inserted into the database.
*/

using System;

namespace GPNBA01Server
{
    public enum State
    {
        PENDING, AVAILABLE
    }

    /*
    Name: Employee
    Developer(s):Gabrel Paquette and Nathan Bray
    Decription: This class contains everything to do with the employee class.
    */
    class Employee
    {
        private int memberId; //the ID of the employee
        private string fName; //the first name of the employee
        private string lName; //the last name of the employee
        private DateTime dob; //the date of birth of the employee


        /*
        Name: Employee
        Description: This is the constructor for the employee object.
                     It sets all of its fields to defaul values.
        */
        public Employee()
        {
            memberId = 0;
            fName = "";
            lName = "";
            dob = DateTime.Today;
            
        }


        /*
        Name: Employee
        Description: This is the constructor for the employee object.
                     It sets all of its fields to the values passed into it.
        Parameters: string memberID -> this is the ID of the client 
                    string fName -> this is the first name of the client
                    string lName -> this is the last name of the client
                    DateTime dob -> this is the date of birth of the client
        */
        public Employee(int memberID, string fName, string lName, string dob)
        {
            memberId = memberID;
            this.fName = fName;
            this.lName = lName;
            this.dob = DateTime.Parse(dob);
        }


        //return the member id
        //sets the memeber id to a new value
        public int MemberId
        {
            get { return memberId; }
            set { memberId = value; }            
        }


        //return the first name of the employee
        //sets the first name of the employee to a new value
        public string FName
        {
            get { return fName; }
            set { fName = value; }
        }


        //return the last name of the employee
        //sets the last name of the employee to a new value
        public string LName
        {
            get { return lName; }
            set { lName = value; }
        }


        //return the date of birth of the employee
        //sets the date of birth of the employee to a new value
        public string DOB
        {
            get { return dob.ToString("MM-dd-yyyy"); }
            set { dob = DateTime.Parse(value); }
        }
    }
}
