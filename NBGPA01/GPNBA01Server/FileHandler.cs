﻿/*
File: FileHander.cs
Project: NBGPA01
Developer(s): Nathan Bray and Gabriel Paquette
Date created: September 20, 2016
Description: This file contains the class that has the functionality to read 
             and write the database to a file as well as write any activity of 
             the server to a log file.
*/


using System;
using System.Timers;
using System.Xml;
using System.Collections;
using System.IO;
using BWCS.Messenger;

namespace GPNBA01Server
{
    /*
    Name: FileHandler
    Developer(s):Gabrel Paquette and Nathan Bray
    Decription: This class contains the functionality to read and write the database to a file
                as well as write any activity of the server to a log file.
    */
    class FileHandler
    {
        private Employee record;
        private const string fileName = "../../EmployeeRecords.xml"; //this is the path where the 
                                                                     //data base is saved
        private const string logFilePath = "logFile.txt"; //the name/path of the log file
        private static object dbLock = new object();      //the object used for the lock
        private static object logLock = new object();   //the object used for the other lock



        public FileHandler()
        {
            record = new Employee();
        }


        /*
        Name: writeToDB
        Description: the method gets called every 3 seconds using a timer
                     it writes teh database to a flat file on the server computer
        Parameters: DataStructure ht -> this is the hashtable that will be writen
                                        to the file on the computer.
                    **NOTE**
                       "object source" and "ElapsedEventArgs e" are needed for the timer
                       to activate properly and to call the correct method.
                    **NOTE**
        Sources: Murach's C# 2012 book by Joal Murach and Ann Boehm
        */
        public void writeToDB(object source, ElapsedEventArgs e, DataStructure ht)
        {
            XmlWriter xmlOut = null;
            try
            {
                //initalises the settings for the xml document.
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = "     ";

                //locks the DB, so nothing can interfere when writing to the flat file
                lock (dbLock)
                {
                    //creates the xml file, if one exists, it writes over it
                    //using the settings specified
                    xmlOut = XmlWriter.Create(fileName, settings);

                    //starts teh document with the tag "RecordList"
                    xmlOut.WriteStartDocument();
                    xmlOut.WriteStartElement("RecordList");
                    ICollection recordCol = ht.Records.Values;

                    //iterates through the hashtable, writing each entry to the file
                    //tagging each field.
                    foreach (Employee record in recordCol)
                    {
                        //specifies the start of the entry
                        xmlOut.WriteStartElement("Employee");
                        xmlOut.WriteAttributeString("MemberID", record.MemberId.ToString());
                        xmlOut.WriteElementString("FirstName", record.FName);
                        xmlOut.WriteElementString("LastName", record.LName);
                        xmlOut.WriteElementString("DOB", record.DOB);
                        //specifies the end of the entry
                        xmlOut.WriteEndElement();
                    }

                    //when done writing, this writes the end of document tag
                    xmlOut.WriteEndElement();
                    //logs this action
                    log(Status.DBSAVE);
                }
            }
            catch (Exception ex)
            {
                //loggs any errors that happened when trying to save the database
                log(Status.ERROR, "Could not safely save database.");
                Console.WriteLine(DateTime.Now + " " + ex.Message);
            }
            finally
            {
                //if the xmlfile was not closed, close it
                if (xmlOut != null)
                {
                    xmlOut.Close();
                }
            }

        }


        /*
        Name: readDB
        Description: this method checks if there is an existing database. If there is
                     then it reads it in, so if any new entries get added, they are given the
                     correct ID and position in the database.
        Return: returns the newly populated database
        Sources: Murach's C# 2012 book by Joal Murach and Ann Boehm
        */
        public DataStructure readDB()
        {
            DataStructure startTable = new DataStructure();
            if (File.Exists(fileName))
            {

                // Create and define the XMLReaderSettings
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreComments = true;
                settings.IgnoreWhitespace = true;
                XmlReader xmlReader = null;
                // Create the XMLReader object using the fileName and the settings
                try
                {
                    //creates an xmlreader variable, for the specified path 
                    //and the specified settings
                    xmlReader = XmlReader.Create(fileName, settings);

                    //decends the reader to the first instance of the "Employee" tag
                    //this is where the database starts.
                    if (xmlReader.ReadToDescendant("Employee"))
                    {

                        //reads through the database until there aren't 
                        //any more Employee tags to read
                        do
                        {
                            //creates a new employee object
                            Employee newMember = new Employee();
                            //reads in the id, converts it to an int and saves it to the new object
                            newMember.MemberId = Convert.ToInt32(xmlReader["MemberID"]);
                            xmlReader.ReadStartElement("Employee");

                            //reads in the first, last name, and the data of birth of 
                            //the employee and saves them to the object
                            newMember.FName = xmlReader.ReadElementContentAsString();
                            newMember.LName = xmlReader.ReadElementContentAsString();
                            newMember.DOB = xmlReader.ReadElementContentAsString();

                            //using the newly saved information, we add the new empoyee to the database
                            startTable.addEntry(newMember.MemberId, newMember.FName, newMember.LName, newMember.DOB);
                        } while (xmlReader.ReadToNextSibling("Employee"));
                    }
                }
                catch (Exception)
                {
                    //if any errors occur during the reading, log them
                    log(Status.ERROR, "Error trying to read from previous database. Potentially corrupted so data may be lost.");
                }
                finally
                {
                    //if the xmlfile was not closed, close it
                    if (xmlReader != null)
                    {
                        xmlReader.Close();
                    }
                }
            }

            //returns the newly populated hashtable
            return startTable;
        }


        /*
        Name: log
        Description:
        Parameters: Status code -> this is the type of message to be logged.
                    string message -> this is the message to be logged if 
                                      an error occured.
        Sources: Murach's C# 2012 book by Joal Murach and Ann Boehm
        */
        public static void log(Status code, string message = "")
        {
            lock (logLock)
            {
                FileStream logStream = null;
                StreamWriter writer = null;
                //starts each log with the date and time
                string logMessage = DateTime.Now.ToString() + " : ";

                //determines what kind of log message to write via the status code passed in
                switch (code)
                {
                    //concatanates the log message with the correct message
                    case Status.DBSAVE:
                        logMessage += "DataBase Backup was successfully done.";
                        break;
                    case Status.ADD:
                        logMessage += "Created a new user.";
                        break;
                    case Status.UPDATE:
                        logMessage += "An entry has been Updated.";
                        break;
                    case Status.DELETE:
                        logMessage += "User Deleted.";
                        break;
                    case Status.ERROR:
                        logMessage += "Error: " + message;
                        break;
                    default:
                        break;
                }

                //try to add it to the log file
                try
                {
                    //gives the file stream the correct path, writing mode and access
                    logStream = new FileStream(logFilePath, FileMode.Append, FileAccess.Write);
                    writer = new StreamWriter(logStream);
                    //writes the log message to the stream, which writes it to the log.
                    writer.WriteLine(logMessage);
                }
                catch (Exception e)
                {
                    //if there was an error, write the error to the console
                    Console.WriteLine("Could not write to log file: " + e.Message);
                }
                finally
                {
                    //closes everything down if it didn't close itself
                    if (writer != null)
                    {
                        writer.Close();
                        if (logStream != null)
                        {
                            logStream.Close();
                        }
                    }
                }
            }
        }
    }
}