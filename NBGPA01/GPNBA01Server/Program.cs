﻿/*
 * File         : Program.cs
 * Project      : RD A01
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2016-09-20
 * Description  : This program is a server application to handle incoming messages from clients
 *                  To handle multiple messages, it threads connections and processes each request 
 *                  on a new thread
 */ 
using System;
using System.Net;
using System.IO;
using System.Net.Sockets;
using BWCS.Messenger;
using BWCS.TCPHandler;
using System.Threading;

namespace GPNBA01Server
{
    class Program
    {
        // Constant for the interval to save to the database, and for the port to connect to
        const int PORT = 8001;
        const int DBSAVE = 3000;

        // Variables to keep track of some metadata
        private static int messageCount;
        private static DateTime uptime;

        // Instantiated object for our Collection and the stress test duration tracker
        private static DataStructure serverCollection = new DataStructure();       
        private static DateTime stressDuration = new DateTime();

        // Instantiated object for the timer to save after duration has passed
        private static System.Timers.Timer myTimer = new System.Timers.Timer(DBSAVE);

        static void Main(string[] args) 
        {
            // Initialize metadata objects
            uptime = DateTime.Now;
            messageCount = 0;

            FileHandler fHandler = new FileHandler();

            TcpListener listener = null;            
            IPAddress ipAddress = null;

            // Read the database file (if it exists)
            serverCollection = fHandler.readDB();

            // And start the timer for saving to the database file
            startTimer(serverCollection, fHandler, myTimer);
            try
            {
                // Get the ip of the WINDOWS machine the server is running on
                string localIP = TCPHandler.GetLocalIPAddress();

                if (IPAddress.TryParse(localIP, out ipAddress))
                {
                    // Listen for TCPClient Requests on PORT
                    listener = new TcpListener(ipAddress, PORT);
                    listener.Start();
                    Console.WriteLine("Server started...");
                    Console.WriteLine("Waiting for incoming client connections...");
                    // Infinately take in connections and process the message
                    while (true)
                    {         
                        // Start the stress time now in case it is a stress test sent               
                        stressDuration = DateTime.Now;

                        // Start threads when connections come in to process the request
                        TcpClient client = listener.AcceptTcpClient();
                        Console.WriteLine(DateTime.Now +" Accepted new client connection...");
                        Thread t = new Thread(ProcessClientRequests);
                        t.Start(client);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }


        /*
         * Name: ProcessClientRequests
         * Description: This method processes the request sent from a client and sends back a response
         * Parameters: object argument - the connection the thread is made to handle
         */
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = null;
            StreamReader reader = null;
            StreamWriter writer = null;
            try
            {
                client = (TcpClient)argument;
                reader = new StreamReader(client.GetStream());
                writer = new StreamWriter(client.GetStream());

                // An array to hold each comma separated value from the message
                string[] requestContent = new string[Messenger.ARGUMENTCOUNT];

                string requestMessage = "";
                string response = "";

                // Read lines from the stream until there are no more lines to read
                while ((requestMessage = reader.ReadLine()) != null)
                {
                    // Increase the number of messages metadata value
                    messageCount += 1;

                    // Parse through the message into the array
                    requestContent = Messenger.parseMessage(requestMessage);

                    // Figure out what action we need to do, and respond with the proper message
                    response = serverAction(requestContent);

                    //Send the response back to the client
                    writer.WriteLine(response);
                    writer.Flush();

                    // if the message was not sent with "9" as the Status, indicating a stress test,
                    // then break. Also, if the stress test responds with an error, then it also leaves
                    if (requestContent[0] != "9" || response.Contains("6, "))
                    {
                        break;
                    }                    
                }
                
                Console.WriteLine(DateTime.Now + " Closing client connection");


            }
            catch (IOException ex)
            {
                Console.WriteLine((int)Status.ERROR+",Problem with client communication. Exiting thread.");
                FileHandler.log(Status.ERROR, ex.Message);
            }
            finally
            {
                // Finally, the cascading closing of objects
                if (client != null)
                {
                    client.Close();
                    if (reader != null)
                    {
                        reader.Close();
                        if (writer != null)
                        {
                            writer.Close();
                        }
                    }
                }
            }
        }

        /*
         * Name: startTimer
         * Description: This method sets timer settings
         * Parameters: DataStructure serverCollection - the collection being sent to write to the file
         *             FileHandler fHandler - the object that reads and writes from files
         *             System.Timers.Timer myTimer - the timer this method sets settings for 
         */
        public static void startTimer(DataStructure serverCollection, FileHandler fHandler, System.Timers.Timer myTimer)
        {
            // everytime the duration is met, call the WriteToDB event, and keep resetting the timer
            myTimer.Elapsed += (sender, e) => fHandler.writeToDB(sender, e, serverCollection);
            myTimer.AutoReset = true;
            myTimer.Enabled = true; 
        }


        /*
         * Name: serverAction
         * Description: This method is the bulk of the program; It will take the message given, parse through it 
         *              for what kind of message was sent, call the event to do that action, form an according 
         *              response, and return it back to main so it can be sent back to the client
         * Parameters: string[] elements - The array containing each individual part of the message
         * Return: string response - The response to send back to the client
         */
        public static string serverAction(string[] elements)
        {
            string response = "";
            string spamHandler = "";
            int state = Convert.ToInt32(elements[0]);
            //int id = 0;
            switch ((Status)state)
            {
                case Status.SPAM:
                    spamHandler = serverCollection.addEntry(elements[1], elements[2], elements[3]);
                    // If the stress test loses connection, it will throw an exception
                    // in which we catch and form an error message
                    if (spamHandler.Contains("6, "))
                    {
                        response = spamHandler;
                    }
                    else
                    {
                        // If no error, form a message to signify the stress test completed fine
                        response = "9,Stress Test Complete in " + (DateTime.Now - stressDuration).Seconds + " Seconds";
                    }
                    break;
                case Status.ADD:
                    response = serverCollection.addEntry(elements[1], elements[2], elements[3]);
                    break;
                case Status.UPDATE:
                    response = serverCollection.updateEntry(elements);
                    break;
                case Status.FIND:
                    response = serverCollection.findEntry(elements[1]);
                    break;
                case Status.DELETE:
                    response = serverCollection.deleteEntry(elements[1]);
                    break;
                case Status.ADMIN:
                    TimeSpan formatDate = DateTime.Now - uptime;
                    TimeSpan newFormat = new TimeSpan(formatDate.Hours, formatDate.Minutes, formatDate.Seconds);
                    response = "5," + serverCollection.Records.Count +","+ messageCount +"," + newFormat.ToString();
                        break;
                case Status.EXIT:
                    // If the exit command is recieved, then we will trigger the elapsed event
                    myTimer.Enabled = true;
                    // Dispose of the timer
                    myTimer.Dispose();
                    // And close the server down
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
            return response;
        }        
    }
}