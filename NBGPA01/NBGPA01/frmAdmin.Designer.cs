﻿namespace NBGPA01
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtServerIP = new System.Windows.Forms.TextBox();
            this.lblServerIP = new System.Windows.Forms.Label();
            this.btnChangeIP = new System.Windows.Forms.Button();
            this.btnLocal = new System.Windows.Forms.Button();
            this.lblIPStatus = new System.Windows.Forms.Label();
            this.btnStressTest = new System.Windows.Forms.Button();
            this.lblForNoOfEntries = new System.Windows.Forms.Label();
            this.lblDBCount = new System.Windows.Forms.Label();
            this.btnUpdateStats = new System.Windows.Forms.Button();
            this.lblForMessageCount = new System.Windows.Forms.Label();
            this.lblMessageCount = new System.Windows.Forms.Label();
            this.btnCloseServer = new System.Windows.Forms.Button();
            this.lblServerUp = new System.Windows.Forms.Label();
            this.lblUptime = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtServerIP
            // 
            this.txtServerIP.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtServerIP.Location = new System.Drawing.Point(123, 6);
            this.txtServerIP.Name = "txtServerIP";
            this.txtServerIP.Size = new System.Drawing.Size(109, 20);
            this.txtServerIP.TabIndex = 0;
            this.txtServerIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtServerIP.TextChanged += new System.EventHandler(this.txtServerIP_TextChanged);
            // 
            // lblServerIP
            // 
            this.lblServerIP.AutoSize = true;
            this.lblServerIP.Location = new System.Drawing.Point(12, 9);
            this.lblServerIP.Name = "lblServerIP";
            this.lblServerIP.Size = new System.Drawing.Size(105, 13);
            this.lblServerIP.TabIndex = 13;
            this.lblServerIP.Text = "Server\'s IP Address: ";
            // 
            // btnChangeIP
            // 
            this.btnChangeIP.Location = new System.Drawing.Point(259, 4);
            this.btnChangeIP.Name = "btnChangeIP";
            this.btnChangeIP.Size = new System.Drawing.Size(75, 23);
            this.btnChangeIP.TabIndex = 1;
            this.btnChangeIP.Text = "Change";
            this.btnChangeIP.UseVisualStyleBackColor = true;
            this.btnChangeIP.Click += new System.EventHandler(this.btnChangeIP_Click);
            // 
            // btnLocal
            // 
            this.btnLocal.Location = new System.Drawing.Point(340, 3);
            this.btnLocal.Name = "btnLocal";
            this.btnLocal.Size = new System.Drawing.Size(75, 23);
            this.btnLocal.TabIndex = 5;
            this.btnLocal.Text = "Local PC";
            this.btnLocal.UseVisualStyleBackColor = true;
            this.btnLocal.Click += new System.EventHandler(this.btnLocal_Click);
            // 
            // lblIPStatus
            // 
            this.lblIPStatus.AutoSize = true;
            this.lblIPStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIPStatus.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblIPStatus.Location = new System.Drawing.Point(123, 33);
            this.lblIPStatus.Name = "lblIPStatus";
            this.lblIPStatus.Size = new System.Drawing.Size(0, 13);
            this.lblIPStatus.TabIndex = 6;
            // 
            // btnStressTest
            // 
            this.btnStressTest.Location = new System.Drawing.Point(111, 226);
            this.btnStressTest.Name = "btnStressTest";
            this.btnStressTest.Size = new System.Drawing.Size(75, 23);
            this.btnStressTest.TabIndex = 3;
            this.btnStressTest.Text = "Stress Test";
            this.btnStressTest.UseVisualStyleBackColor = true;
            this.btnStressTest.Click += new System.EventHandler(this.btnStressTest_Click);
            // 
            // lblForNoOfEntries
            // 
            this.lblForNoOfEntries.AutoSize = true;
            this.lblForNoOfEntries.Location = new System.Drawing.Point(13, 128);
            this.lblForNoOfEntries.Name = "lblForNoOfEntries";
            this.lblForNoOfEntries.Size = new System.Drawing.Size(121, 13);
            this.lblForNoOfEntries.TabIndex = 11;
            this.lblForNoOfEntries.Text = "Number of entries in Db:";
            // 
            // lblDBCount
            // 
            this.lblDBCount.AutoSize = true;
            this.lblDBCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDBCount.Location = new System.Drawing.Point(183, 117);
            this.lblDBCount.Name = "lblDBCount";
            this.lblDBCount.Size = new System.Drawing.Size(0, 24);
            this.lblDBCount.TabIndex = 8;
            // 
            // btnUpdateStats
            // 
            this.btnUpdateStats.Location = new System.Drawing.Point(12, 226);
            this.btnUpdateStats.Name = "btnUpdateStats";
            this.btnUpdateStats.Size = new System.Drawing.Size(93, 23);
            this.btnUpdateStats.TabIndex = 2;
            this.btnUpdateStats.Text = "Update Stats";
            this.btnUpdateStats.UseVisualStyleBackColor = true;
            this.btnUpdateStats.Click += new System.EventHandler(this.btnUpdateStats_Click);
            // 
            // lblForMessageCount
            // 
            this.lblForMessageCount.AutoSize = true;
            this.lblForMessageCount.Location = new System.Drawing.Point(13, 177);
            this.lblForMessageCount.Name = "lblForMessageCount";
            this.lblForMessageCount.Size = new System.Drawing.Size(144, 13);
            this.lblForMessageCount.TabIndex = 10;
            this.lblForMessageCount.Text = "Total requests sent to server:";
            // 
            // lblMessageCount
            // 
            this.lblMessageCount.AutoSize = true;
            this.lblMessageCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageCount.Location = new System.Drawing.Point(183, 169);
            this.lblMessageCount.Name = "lblMessageCount";
            this.lblMessageCount.Size = new System.Drawing.Size(0, 24);
            this.lblMessageCount.TabIndex = 9;
            // 
            // btnCloseServer
            // 
            this.btnCloseServer.Location = new System.Drawing.Point(322, 226);
            this.btnCloseServer.Name = "btnCloseServer";
            this.btnCloseServer.Size = new System.Drawing.Size(75, 23);
            this.btnCloseServer.TabIndex = 4;
            this.btnCloseServer.Text = "Close Server";
            this.btnCloseServer.UseVisualStyleBackColor = true;
            this.btnCloseServer.Click += new System.EventHandler(this.btnCloseServer_Click);
            // 
            // lblServerUp
            // 
            this.lblServerUp.AutoSize = true;
            this.lblServerUp.Location = new System.Drawing.Point(9, 76);
            this.lblServerUp.Name = "lblServerUp";
            this.lblServerUp.Size = new System.Drawing.Size(104, 13);
            this.lblServerUp.TabIndex = 12;
            this.lblServerUp.Text = "Total Server Uptime:";
            // 
            // lblUptime
            // 
            this.lblUptime.AutoSize = true;
            this.lblUptime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUptime.Location = new System.Drawing.Point(182, 67);
            this.lblUptime.Name = "lblUptime";
            this.lblUptime.Size = new System.Drawing.Size(0, 25);
            this.lblUptime.TabIndex = 7;
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 261);
            this.Controls.Add(this.lblUptime);
            this.Controls.Add(this.lblServerUp);
            this.Controls.Add(this.btnCloseServer);
            this.Controls.Add(this.lblMessageCount);
            this.Controls.Add(this.lblForMessageCount);
            this.Controls.Add(this.btnUpdateStats);
            this.Controls.Add(this.lblDBCount);
            this.Controls.Add(this.lblForNoOfEntries);
            this.Controls.Add(this.btnStressTest);
            this.Controls.Add(this.lblIPStatus);
            this.Controls.Add(this.btnLocal);
            this.Controls.Add(this.btnChangeIP);
            this.Controls.Add(this.lblServerIP);
            this.Controls.Add(this.txtServerIP);
            this.Name = "frmAdmin";
            this.Text = "Admin Panel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtServerIP;
        private System.Windows.Forms.Label lblServerIP;
        private System.Windows.Forms.Button btnChangeIP;
        private System.Windows.Forms.Button btnLocal;
        private System.Windows.Forms.Label lblIPStatus;
        private System.Windows.Forms.Button btnStressTest;
        private System.Windows.Forms.Label lblForNoOfEntries;
        private System.Windows.Forms.Label lblDBCount;
        private System.Windows.Forms.Button btnUpdateStats;
        private System.Windows.Forms.Label lblForMessageCount;
        private System.Windows.Forms.Label lblMessageCount;
        private System.Windows.Forms.Button btnCloseServer;
        private System.Windows.Forms.Label lblServerUp;
        private System.Windows.Forms.Label lblUptime;
    }
}