﻿/*
 * File         : frmAdmin.cs
 * Project      : RD A01
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2016-09-13
 * Description  : This partial class for the admin panel handles the events caused within the admin window.
 *                  This includes: Changing the ip, shutting off the server, showing some metadata, and a 
 *                  stress test feature
 */ 
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using BWCS.Messenger;
using BWCS.TCPHandler;

namespace NBGPA01
{
    public partial class frmAdmin : Form
    {
        private bool isClosed = false;

        public frmAdmin()
        {
            InitializeComponent();
            // This will change the text of the "Server IP" to what the 
            // default IP is set to. (Current PC IP address)
            txtServerIP.Text = TCPHandler.GetLocalIPAddress();
        }


        /*
         * Name: btnChangeIP_Click
         * Description: This method will change  the "Server IP" to what the user sets it to.
         *              Then it will call updateStats, to both check if a connection is valid,
         *              and to try to update the metadata
         * Parameters: sender,e
         */
        private void btnChangeIP_Click(object sender, EventArgs e)
        {
            this.Tag = txtServerIP.Text;
            TCPHandler.ServerIp = txtServerIP.Text;
            // Display to the user they changed something
            lblIPStatus.Text = "Changed";

            // Invoke the event to update the metadata
            btnUpdateStats_Click(sender, e);
        }


        /*
         * Name: btnLocal_Click
         * Description: This method will set the ip address the client thinks the server is on to the ip address
         *               of the PC it is running on
         * Parameters: sender,e
         */
        private void btnLocal_Click(object sender, EventArgs e)
        {
            txtServerIP.Text = TCPHandler.GetLocalIPAddress();
            // Invoke the event that changes the ip known by the server using the text we just put into 
            // the text field
            btnChangeIP_Click(sender, e);
        }


        /*
         * Name: txtServerIP_TextChanged
         * Description: This method will trigger every key press or character change in the text field,
         *              to make sure that the input can only consist of number and periods.
         *              The reasoning here being that an Administrator should know better than to incorrrectly
         *              put in an ip, but we'll help them out as much as possible. The below code is also
         *              altered code from kumarch1 mentioned in frmMain.cs
         * Parameters: sender,e
         */
        private void txtServerIP_TextChanged(object sender, EventArgs e)
        {
            string finalString = string.Empty;
            char[] startString = txtServerIP.Text.ToCharArray();
            // For every character in the string
            foreach (char curChar in startString.AsEnumerable())
            {
                //check if it is a digit or a period
                if (Char.IsDigit(curChar) || curChar == '.')
                {
                    finalString = finalString + curChar;
                }
                else
                {
                    // If not, make it a space and trim it out of the string
                    finalString.Replace(curChar, ' ');
                    finalString.Trim();
                }
            }
            //posting the newly formatted string back in the text field
            txtServerIP.Text = finalString;
        }


        /*
         * Name: btnStressTest_Click
         * Description: This method handles the event when the user wants to stress test the server.
         * Parameters: sender,e
         */
        private void btnStressTest_Click(object sender, EventArgs e)
        {
            string response = "";
            // This triggers the method to spam messages at the server
            response = TCPHandler.sendStressMsg();

            string[] array = new string[Messenger.ARGUMENTCOUNT];
            array = Messenger.parseMessage(response);

            // This handles the response of the spam, whether it be error or successfully handled
            int state = Convert.ToInt32(array[0]);
            switch ((Status)state)
            {
                case Status.ERROR:
                    MessageBox.Show(array[1], "Error");
                    break;
                case Status.SPAM:
                    MessageBox.Show(array[1], "Stress Results");
                    // And if the test was successful, we will update the metadata as well
                    btnUpdateStats_Click(sender, e);
                    break;
                default:
                    break;
            }
            
        }


        /*
         * Name: btnUpdateStats_Click
         * Description: This method sends an "Admin" message to the server, and retrieves some metadata like
         *              server uptime, total entries in the database, and how many messages were sent to the 
         *              server since it went up
         * Parameters: sender,e
         */
        private void btnUpdateStats_Click(object sender, EventArgs e)
        {
            string response = TCPHandler.sendMsg("5");
            string[] array = new string[5];

            array = Messenger.parseMessage(response);
            int state = Convert.ToInt32(array[0]);

            // Generally, an error happens when trying to retrieve from a connection that either ended
            // or is using the wrong ip
            if (state == (int)Status.ERROR)
            {
                MessageBox.Show(array[1]+"\nThe server may be offline, please check with your local administrator", "Error");
            }
            else
            {
                // Populate the labels accordingly
                lblDBCount.Text = array[1];
                lblMessageCount.Text = array[2];
                lblUptime.Text = array[3];
            }            
        }


        /*
         * Name: btnCloseServer_Click
         * Description: This method will update the metadata one more time before telling the server to close
         * Parameters: sender,e
         */
        private void btnCloseServer_Click(object sender, EventArgs e)
        {
            btnUpdateStats_Click(sender, e);
            string response = TCPHandler.sendMsg("8");

            // If the bool has been set to true already, thn it won't run this
            // Thus making the server shutting down message only appear once
            if (!isClosed)
            {
                MessageBox.Show("Server shutting down.");
                isClosed = true;
            }
        }
    }
}
