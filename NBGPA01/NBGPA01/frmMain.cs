﻿/*
 * File         : Form1.cs
 * Project      : RD A01
 * Developer(s) : Nathan Bray, Gabriel Paquette
 * Date Created : 2016-09-13
 * Description  : This file contains the main client program for this client/server program.
 *                It will let users do the basic CRUD command on the server, as well as access 
 *                an "Admin only" portal to access additional features
 */ 
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using BWCS.Messenger;
using BWCS.TCPHandler;

namespace NBGPA01
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            // When the program loads, we want our DateTime picker o be in this specific format
            dtDOB.Format = DateTimePickerFormat.Custom;
            dtDOB.CustomFormat = "MMMM.dd, yyyy";

            // And, we are going to set it to a max date of today
            dtDOB.MaxDate = DateTime.Today;
        }


        /*
         * Name: btnAdd_Click
         * Description: This method prepares the client to send an "Add" request, and makes
         *              various parts of the window activate for editting purposes
         * Parameters: sender,e
         */
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Messenger.Status = Status.ADD;
            txtMemberID.Clear();
            txtMemberID.Enabled = false;
            lblID.Enabled = false;

            pnlFields.Enabled = true;
            pnlFormButtons.Enabled = true;
        }


        /*
         * Name: btnAdmin_Click
         * Description: This method invokes the Admin window to appear
         * Parameters: sender,e
         */
        private void btnAdmin_Click(object sender, EventArgs e)
        {
            Form adminPanel = new frmAdmin();
            adminPanel.ShowDialog();
        }


        /*
         * Name: btnUpdate_Click
         * Description: This method changes the clients view to send update messages instead
         * Parameters: sender,e
         */
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Messenger.Status = Status.UPDATE;
            txtMemberID.Enabled = true;
            lblID.Enabled = true;
            pnlFields.Enabled = true;
            pnlFormButtons.Enabled = true;
        }


        /*
         * Name: btnCancel_Click
         * Description: This method cancels the entry they were trying to make, clearing the form 
         * Parameters: sender,e
         */
        private void btnCancel_Click(object sender, EventArgs e)
        {
            clearForm();
            txtMemberID.Enabled = false;
            lblID.Enabled = false;
            pnlFields.Enabled = false;
            pnlFormButtons.Enabled = false;
        }


        /*
         * Name: clearForm
         * Description: This clears all input fields, and sets the date to today
         */
        private void clearForm()
        {
            txtMemberID.Clear();
            txtFirstName.Clear();
            txtLastName.Clear();
            dtDOB.Value = dtDOB.MaxDate;
        }

        
        /*
         * Name: btnReset_Click
         * Description: This calls clearForm when the user wants to reset the form
         * Parameters: sender,e
         */
        private void btnReset_Click(object sender, EventArgs e)
        {
            clearForm();
        }


        /*
         * Name: btnFind_Click
         * Description: This sets the client to be sending a 'Find' message
         * Parameters: sender,e
         */
        private void btnFind_Click(object sender, EventArgs e)
        {
            Messenger.Status = Status.FIND;            
            txtMemberID.Enabled = true;
            lblID.Enabled = true;
            pnlFields.Enabled = false;
            pnlFormButtons.Enabled = true;
        }


        /*
         * Name: validateForm
         * Description: This method will change the text of the "Server IP" to what the default IP is set to. (Currently localhost)
         * Return: bool valid - Whether it is valid input or not
         */
        private bool validateForm()
        {
            bool valid = false;
            string err = "";
            // For update, we also need to validate the ID 
            // It is validated up here as the C# language took out the ability to do an 
            // intentional fall through in switch statements
            if (Messenger.Status == Status.UPDATE)
            {
                err += checkID();
            }
            switch (Messenger.Status)
            {
                case Status.FIND:
                case Status.DELETE:
                    err += checkID();
                    break;
                case Status.UPDATE:
                case Status.ADD:
                    // Check if the strings are empty
                    if (String.IsNullOrWhiteSpace(txtFirstName.Text.Trim()))
                    {
                        err += "Enter a First Name. \n";
                    }
                    if (String.IsNullOrWhiteSpace(txtLastName.Text.Trim()))
                    {
                        err += "Enter a Last Name. \n";
                    }
                    break;
                default:
                    break;
            }
            // If the error message is empty, we can return true
            if (String.IsNullOrWhiteSpace(err))
            {
                valid = true;
            }
            else
            { 
                // Otherwise, inform the user
                MessageBox.Show(err, "Form Error");
            }
            return valid;
        }


        /*
         * Name: btnDelete_Click
         * Description: This sets the client to send a delete message after the client hits submit
         * Parameters: sender,e
         */
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Messenger.Status = Status.DELETE;
            txtMemberID.Enabled = true;
            lblID.Enabled = true;
            pnlFields.Enabled = false;
            pnlFormButtons.Enabled = true;

        }


        /*
         * Name: btnSubmit_Click
         * Description: This contains the majority of the client side logic.
         *              First, it will make sure all necessary fields are validated (dependant on the Status)
         *              Then, it will form a message and attempt to send it to where it thinks the server is.
         *              Then, it will parse the recieved messae into an array, and check the returned Status code
         *              Finally, informing the user of what happened
         * Parameters: sender,e
         */
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string msg = "";
            if (validateForm())
            {
                msg = Messenger.makeMessage(Messenger.Status, txtMemberID.Text, txtFirstName.Text, txtLastName.Text, dtDOB.Value);

                msg = TCPHandler.sendMsg(msg);

                string[] array = new string[Messenger.ARGUMENTCOUNT];
                array = Messenger.parseMessage(msg);

                // After the message is in an array, we format the responses (depending on Status)
                msg = Messenger.responseFormat(array); 

                if (msg.Trim() != "")
                {
                    // The first part of the array, is converted to the int form of the Status enum
                    int state = Convert.ToInt32(array[0]);
                    // And then compared to handle the types of messages 
                    switch ((Status)state)
                    {
                        case Status.ADD:
                            MessageBox.Show(msg, "Entry successfully added");
                            break;
                        case Status.ERROR:
                            MessageBox.Show(msg, "Server Error Occurred");
                            break;
                        case Status.UPDATE:
                            MessageBox.Show(msg, "Your Update results");
                            break;
                        case Status.FIND:
                            MessageBox.Show(msg, "Your search results");
                            break;
                        case Status.DELETE:
                            MessageBox.Show(msg, "Entry deletion Results");
                            break;
                        default:
                            MessageBox.Show(msg, "Server Response");
                            break;
                    }
                } 
            }
        }


        /*
         * Name: checkID
         * Description: Checks the ID field to see if it is empty or 0, or a number greater than a 32bit integer can handle
         * Return: string - the error message if any
         */
        private string checkID()
        {
            string id = txtMemberID.Text;
            string errMsg = "";
            // If it is empty
            if (String.IsNullOrWhiteSpace(id))
            {
                errMsg = "Please enter a Member ID in the field provided.";
            }
            else
            {
                try
                {
                    // Otherwise try to convert this number
                    if (Convert.ToInt32(id) >= 1)
                    {
                        txtMemberID.Text = id.ToString();
                    }
                    else
                    {
                        errMsg = "Zero is not a valid Member ID, please enter a value greater than 0.";
                    }
                }
                catch (Exception )
                {
                    //If not, it is because they inputted a number higher than a 32bit integer can handle
                    errMsg = "Please specify a smaller number";
                }
            }
            return errMsg;
        }


        // The below TextChanged event code contains altered code originally found at
        // http://stackoverflow.com/questions/463299/how-do-i-make-a-textbox-that-only-accepts-numbers
        // posted by kumarch1 and revised to allow the fields to only accept numbers/letters accordingly

        /*
         * Name: txtMemberID_TextChanged
         * Description: This method will trigger every key press or character change in the text field,
         *              to make sure that the input can only consist of numbers.
         * Parameters: sender,e
         */
        private void txtMemberID_TextChanged(object sender, EventArgs e)
        {
            string finalString = string.Empty;
            char[] startString = txtMemberID.Text.ToCharArray();
            // For every character in the string
            foreach (char curChar in startString.AsEnumerable())
            {
                //check if it is a digit
                if (Char.IsDigit(curChar))
                {
                    finalString = finalString + curChar;
                }
                else
                {
                    // If not, make it a space and trim it out of the string
                    finalString.Replace(curChar, ' ');
                    finalString.Trim();
                }
            }
            // posting the newly formatted string back in the text field
            txtMemberID.Text = finalString;
        }


        /*
         * Name: textFields_TextChanged
         * Description: This method will trigger every key press or character change in the text field,
         *              to make sure that the input can only consist of letters.
         * Parameters: sender,e
         */
        private void textFields_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = ((TextBox)sender);
            string finalString = string.Empty;
            char[] startString = textBox.Text.ToCharArray();
            // For every character in the string
            foreach (char curChar in startString.AsEnumerable())
            {
                //check if it is a letter
                if (Char.IsLetter(curChar))
                {
                    finalString = finalString + curChar;
                }
                else
                {
                    // If not, make it a space and trim it out of the string
                    finalString.Replace(curChar, ' ');
                    finalString.Trim();
                }
            }
            // posting the newly formatted string back in the text field
            textBox.Text = finalString;
        }
    }
}
